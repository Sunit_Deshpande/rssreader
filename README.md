# Backend

Steps to run backend
1) cd rssreader/backend
2) pip install -r requirmentes.txt
3) cd rssreader
4) ./manage.py migrate 
5) ./manage.py runserver 

## Todo:
	- User Managment
	- Authentication

# Frontend

Steps to run
1) cd rssreader/frontend
2) yarn install
3) yarn run start:dev

## Todo:
	- Better Error Reporting
	- Improve styling

