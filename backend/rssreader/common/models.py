from django.db import models


class EnumeratedModel(models.Model):
    title = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
