from rest_framework import status
from rest_framework.exceptions import APIException


class InvalidPrimaryKeyException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = ('Invalid pk {pk} for model {model}.')
    default_code = 'invalid_pk'

    def __init__(self, pk="", model="", detail=None, code=None):
        if detail is None:
            detail = self.default_detail.format(pk=pk, model=model)
        super(InvalidPrimaryKeyException, self).__init__(detail, code)


class InvalidTypeException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = ('{parameter} must be of {type} type but found {found}.')
    default_code = 'invalid_value'

    def __init__(self, required_type="", parameter="", found_type="", detail=None, code=None):
        if detail is None:
            detail = self.default_detail.format(parameter=parameter, type=required_type, found=found_type)
        super(InvalidTypeException, self).__init__(detail, code)
