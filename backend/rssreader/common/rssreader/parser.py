import xml.etree.ElementTree as ET


class BaseParser(object):

    def parse(self, content, *args, **kwargs):
        raise NotImplementedError('Sub class of BaseParser must implement parse() method')


class DefaultParser(object):

    def _get_text_or_none(self, parent, element):
        element = parent.find(element)
        if element is None:
            return None
        else:
            return element.text

    def parse(self, content, *args, **kwargs):
        xmltree = ET.fromstring(content)

        item_list = [
            {
                'title': self._get_text_or_none(item, 'title'),
                'description': self._get_text_or_none(item, 'description'),
                'link': self._get_text_or_none(item, 'link'),
                'published_at': self._get_text_or_none(item, 'pubDate'),
                'thumbnail_url': self._get_text_or_none(item, 'media:thumbnail'),
            }

            for item in xmltree.iter('item')
        ]

        return item_list


parser = DefaultParser()
