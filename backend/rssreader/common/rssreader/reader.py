from datetime import datetime

import requests

from content.models import Feed, Website

from .parser import parser


class BaseReader(object):

    def read(self, url, write_to_db=False, *args, **kwargs):
        raise NotImplementedError('Subclass of BaseReader must implement read() function')


class DefaultReader(object):

    def _write_to_db(self, content, url):
        website = Website.objects.get(url=url)

        orginial_count = Feed.objects.filter(website=website).count()

        for item in content:
            published_at = item['published_at']
            if published_at is not None:
                published_at = datetime.strptime(item['published_at'], '%a, %d %b %Y %H:%M:%S %Z')
            Feed.objects.get_or_create(website=website, title=item['title'], description=item['description'],
                                       link=item['link'], published_at=published_at, thumbnail_url=item['thumbnail_url'])

        new_count = Feed.objects.filter(website=website).count()

        if orginial_count == new_count:
            return False

        return True

    def read(self, url, *args, **kwargs):
        content = requests.get(url).text
        parsed_context = parser.parse(content)

        is_updated = self._write_to_db(parsed_context, url)

        return is_updated


reader = DefaultReader()
