from django.contrib import admin

from .models import Website, Feed


class WebsiteAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'url')
    search_fields = ('title',)
    ordering = ('created',)


admin.site.register(Website, WebsiteAdmin)


class FeedAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'link')
    search_fields = ('title',)
    ordering = ('-published_at',)


admin.site.register(Feed, FeedAdmin)
