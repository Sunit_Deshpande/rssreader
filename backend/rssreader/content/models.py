from django.db import models

from common.models import EnumeratedModel, TimeStampedModel


class Website(EnumeratedModel, TimeStampedModel):
    url = models.URLField(blank=False, null=False, unique=True)


class Feed(EnumeratedModel, TimeStampedModel):
    website = models.ForeignKey('content.Website', blank=False, null=False, on_delete=models.CASCADE, related_name='feeds')
    link = models.URLField(null=True, blank=True)

    published_at = models.DateTimeField(null=True, blank=True)
    thumbnail_url = models.URLField(null=True, blank=True)
