from rest_framework import serializers

from .models import Website, Feed


class WebsiteSerialzier(serializers.ModelSerializer):

    class Meta:
        model = Website
        fields = '__all__'


class FeedSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feed
        fields = '__all__'
