from rest_framework_nested import routers

from .views import WebsiteViewSet, WebsiteFeedUpdateViewSet, FeedViewSet

router = routers.SimpleRouter()
router.register(r'websites', WebsiteViewSet, base_name='websites-api')

websiter_router = routers.NestedSimpleRouter(router, r'websites', lookup='websites')
websiter_router.register(r'update-feeds', WebsiteFeedUpdateViewSet, base_name='update-feeds-api')

websiter_router.register(r'feeds', FeedViewSet, base_name='feeds-api')

urlpatterns = router.urls + websiter_router.urls
