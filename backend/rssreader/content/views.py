from django.db import transaction

from rest_framework import viewsets, status
from rest_framework.response import Response

from common.rest_exceptions import InvalidPrimaryKeyException, InvalidTypeException
from common.rssreader.reader import reader

from .models import Website, Feed
from .serializers import WebsiteSerialzier, FeedSerializer


class WebsiteViewSet(viewsets.ModelViewSet):
    queryset = Website.objects.all()
    serializer_class = WebsiteSerialzier


class WebsiteFeedUpdateViewSet(viewsets.ViewSet):

    @transaction.atomic
    def list(self, request, *args, **kwargs):

        website_pk = kwargs.get('websites_pk', None)

        try:
            website = Website.objects.get(id=website_pk)
        except ValueError:
            raise InvalidTypeException(required_type='int', parameter='pk', found_type=type(website_pk).__name__)
        except Website.DoesNotExists:
            raise InvalidPrimaryKeyException(pk=website_pk, model='website')

        is_updated = reader.read(website.url)

        response_data = {
            'is_updated': is_updated
        }
        return Response(response_data, status=status.HTTP_200_OK)


class FeedViewSet(viewsets.ModelViewSet):
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer

    http_method_names = ['get']

    def get_queryset(self):
        website_pk = self.kwargs.get('websites_pk', None)

        try:
            website = Website.objects.get(id=website_pk)
        except ValueError:
            raise InvalidTypeException(required_type='int', parameter='pk', found_type=type(website_pk).__name__)
        except Website.DoesNotExists:
            raise InvalidPrimaryKeyException(pk=website_pk, model='website')

        return self.queryset.filter(website=website).order_by('-published_at')
