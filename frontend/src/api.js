class API {

	constructor(){
		this.serverUrl = 'http://localhost:8000';
	}

	websiteUrl(){
		return {
			list: () => `${this.serverUrl}/api/v1/websites/`,
			detail: (id) => `${this.serverUrl}/api/v1/websites/${id}/`,
		}
	}

	feedsUrl(websiteId){
		return{
			list: () => `${this.websiteUrl().detail(websiteId)}feeds/`,
			update: () => `${this.websiteUrl().detail(websiteId)}update-feeds/`,
		}
	}
}

export default new API();