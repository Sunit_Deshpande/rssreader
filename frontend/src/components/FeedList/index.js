import React from 'react';
import PropTypes from 'prop-types';

import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import ActionAutoReNew from 'material-ui/svg-icons/action/autorenew';
import {fullWhite} from 'material-ui/styles/colors';

import style from './style.scss';

export default class FeedList extends React.Component{


	constructor(props){
		super(props);

		this.handelOnRefereshClicked = this.handelOnRefereshClicked.bind(this);
	}

	handelOnRefereshClicked(){
		this.props.onRefresh();
	}

	render() {

		let header = (
			<header
				className={style.header}>
				<div>
					<span
						className={style.logo}>
							{ this.props.currentWebsite !== null ? this.props.currentWebsite.title: 'Select/Add a Website' }
					</span>
				</div>
				<IconButton
					disabled={this.props.currentWebsite === null} 
					onClick={this.handelOnRefereshClicked}>
					<ActionAutoReNew color={fullWhite} />
				</IconButton>

			</header>
		);
		
		let feedlist = this.props.currentFeeds.map(function(feed){

			return (
				<Paper
					key={feed.id} 
					zDepth={1}
					className={style.feed}>
						<p
							className={style.feedTitle}>
							<a 
								href={feed.link}
								target="_blank">
									{feed.title}
							</a>
						</p>
						<p
							className={style.feedDesc}>
							{feed.description}
						</p>
						<p
							className={style.feedTime}>
							{new Date(feed.published_at).toLocaleString() }
						</p>
				</Paper>
			)
		});

		return (
			<div
				className={style.container}>
				{header}
				<div
					className={style.feedList}>
					{feedlist}
				</div>
			</div>
		);
	}
}

FeedList.propTypes = {
	currentFeeds: PropTypes.array.isRequired,
	currentWebsite: PropTypes.object,
	onRefresh: PropTypes.func.isRequired,
}