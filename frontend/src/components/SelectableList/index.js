import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {List, ListItem, makeSelectable} from 'material-ui/List';

let SelectableList = makeSelectable(List);



function wrapState(ComposedComponent) {

  return class SelectableList extends Component {

    componentWillMount() {
      this.setState({
        selectedIndex: this.props.defaultValue,
      });

      this.handleRequestChange = this.handleRequestChange.bind(this);
    }

    handleRequestChange(event, index){
      this.setState({
        selectedIndex: index,
      });

      this.props.onChange(index);
    };

    render() {
      return (
        <ComposedComponent
          value={this.state.selectedIndex}
          onChange={this.handleRequestChange}>
            {this.props.children}
        </ComposedComponent>
      );
    }
  };
}

export default wrapState(SelectableList);