import React from 'react';

import PropTypes from 'prop-types';

import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import {lightGreenA700, fullWhite, grey400} from 'material-ui/styles/colors';
import { ListItem } from 'material-ui/List';
import Dialog from 'material-ui/Dialog';

import SelectableList from '../SelectableList';

export default class WebsiteList extends React.Component{

	constructor(props){
		super(props);

		this.state = {
			showAddDialog: false,
			websiteUrlValue: '',
			websiteTitleValue: '',
		}

		this.handleListSelectionChange = this.handleListSelectionChange.bind(this);
		this.handelAddOnClick = this.handelAddOnClick.bind(this);
		this.handelAddDialogOnClose = this.handelAddDialogOnClose.bind(this);
		this.handelItemOnDeleteClick = this.handelItemOnDeleteClick.bind(this);
		this.handelAddDialogOnSave = this.handelAddDialogOnSave.bind(this);
	}

	handleListSelectionChange(index){
		this.props.onChange(index);
	}

	handelAddOnClick(){
		this.setState({
			showAddDialog: true
		})
	}

	handelAddDialogOnClose(){
		this.setState({
			showAddDialog: false
		})
	}

	handelAddDialogOnSave(){
		this.setState({
			showAddDialog: false
		});

		this.props.onAdd({
			websiteTitle: this.state.websiteTitleValue,
			websiteUrl: this.state.websiteUrlValue
		});
	}

	handelTextChange(event, key) {
		this.setState({
			[key]: event.target.value
		})
	}

	handelItemOnDeleteClick(item){
		this.props.onDelete(item)
	}

	render() {
		var self=this;

		let websiteList = this.props.websites.map(function(item) {
			return (
				<ListItem 
					key={item.id} 
					value={item.id} 
					primaryText={item.title} 
					rightIconButton={	<IconButton 
											onClick={()=>self.handelItemOnDeleteClick(item)}>
											<ActionDelete 
												color={grey400}/>
										</IconButton>
									} />
			);
		});

		const addDialogActions = [
			<FlatButton
        	label="Cancel"
        	primary={true}
        	onClick={this.handelAddDialogOnClose}/>,

	      <FlatButton
	        label="Save"
	        primary={true}
	        disabled={this.state.websiteUrlValue === '' || this.state.websiteTitleValue === ''}
	        onClick={this.handelAddDialogOnSave}/>,
		]

		return (
			<div>
				<SelectableList 
					onChange={(index) => this.handleListSelectionChange(index)}>
					{websiteList}
				</SelectableList>
				<RaisedButton 
					backgroundColor={lightGreenA700} 
					fullWidth={true}
					onClick={this.handelAddOnClick} 
					icon={<ContentAdd color={fullWhite}/>}/>
				<Dialog
					title='Add RSS Feed'
					open={this.state.showAddDialog}
					modal={false}
					actions={addDialogActions}
					onRequestClose={this.handelAddDialogOnClose}>
						<TextField
							fullWidth={true}
							onChange={(event) => this.handelTextChange(event, 'websiteTitleValue')}
					    	floatingLabelText="Website title"/><br/>
  						<TextField
  							type="url"
  							onChange={(event) => this.handelTextChange(event, 'websiteUrlValue')}
							fullWidth={true}
					    	floatingLabelText="Website URL"/>
				</Dialog>
			</div>
		);
	}
}

WebsiteList.propTypes = {
	websites: PropTypes.array.isRequired,
	currentWebsite: PropTypes.object,
	onChange: PropTypes.func.isRequired,
	onAdd: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired,
}