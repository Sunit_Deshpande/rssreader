import React from 'react';
import ReactDOM from 'react-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import LandingPage from './pages/LandingPage'

import style from './styles/app.scss';

class App extends React.Component{

	render(){
		return (
			<MuiThemeProvider>
				<LandingPage />
			</MuiThemeProvider>
		)
	}
}

ReactDOM.render(<App/>, document.getElementById('app'));