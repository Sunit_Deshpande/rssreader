import React from 'react';

import Paper from 'material-ui/Paper';
import Snackbar from 'material-ui/Snackbar';

import WebsiteList from '../../components/WebsiteList';
import FeedList from '../../components/FeedList';

import RssReader from '../../utils/RssReader';

import style from './style.scss';

export default class LandingPage extends React.Component {

	constructor(props){
		super(props);

		this.state = {
			websites: [],
			currentWebsite: null,
			currentFeeds: [],
			snackbar: {
				isOpen: false,
				message: '',
			}
		}

		this.updateWebstieList = this.updateWebstieList.bind(this);
		this.updateCurrentFeed = this.updateCurrentFeed.bind(this);
		this.deleteWebsite = this.deleteWebsite.bind(this);
		this.addWebsite = this.addWebsite.bind(this);
		this.updateFeedList = this.updateFeedList.bind(this);
		this.handleSnackbarClose = this.handleSnackbarClose.bind(this);

		this.updateWebstieList();

	}


	updateWebstieList(){
		RssReader.getAllWebsites()
			.then((data) => {
				this.setState({
					websites: data
				})

			})
			.catch((error) => console.log(error));

	}

	updateFeedList(){
		let self = this;
		RssReader.updateFeeds(this.state.currentWebsite.id)
			.then((data)=> {
				if(data.is_updated==true){
					self.updateCurrentFeed(this.state.currentWebsite.id)
					self.setState({
						snackbar: {
							isOpen: true,
							message: 'New feed avaliable',
						}
					})
				}else{
					self.setState({
						snackbar: {
							isOpen: true,
							message: 'No updates.',
						}
					})
				}
			})
			.catch((error)=> console.log(error));
	}

	updateCurrentFeed(websiteId){
		let website = this.state.websites.find(item => item.id === websiteId);
		RssReader.getAllFeeds(websiteId)
			.then((data)=> 
				this.setState({
					currentFeeds: data,
					currentWebsite: website,
				})
			)
			.catch((error) => console.log(error));
	}

	addWebsite(data){
		let self = this;
		let { websiteTitle, websiteUrl } = data;

		RssReader.addWebsite(websiteTitle, websiteUrl)
			.then((data) => {
				self.setState({
					websites: [...self.state.websites, data],
					snackbar: {
						isOpen: true,
						message: 'Website Added'
					}
				})

			})
			.catch((error) => console.log(error));
	}

	deleteWebsite(website){
		let self = this;
		RssReader.deleteWebsite(website.id)
			.then((data) => {
				let websites = self.state.websites;
				websites.splice(websites.indexOf(website), 1);
				
				let currentWebsite = this.state.currentWebsite.id === website.id? null: this.state.currentWebsite;
				let currentFeeds = this.state.currentWebsite.id === website.id? []: this.state.currentFeeds;

				self.setState({
					websites,
					currentFeeds,
					currentWebsite,
					snackbar: {
						isOpen: true,
						message: `Deleted Website - ${website.title}`,
					}
				});

			})
			.catch((error)=> console.log(error));
	}

	handleSnackbarClose(){
		this.setState({
			snackbar:{
				isOpen: false,
			}
		})
	}

	render(){

		return (
			<main 
				className={style.main}>

				<Paper 
					className={style.aside} 
					zDepth={1}>
					<header 
						className={style.header}>
						<span 
							className={style.logo}>WebSites</span>
					</header>

					<WebsiteList 
						websites={this.state.websites} 
						currentWebsite={this.state.currentWebsite} 
						onChange={this.updateCurrentFeed}
						onAdd={this.addWebsite}
						onDelete={this.deleteWebsite}/>
				
				</Paper>
			
				<section 
					className={style.section}>
					<FeedList 
						currentFeeds={this.state.currentFeeds} 
						currentWebsite={this.state.currentWebsite}
						onRefresh={this.updateFeedList}/>
				</section>	
				<Snackbar
          			open={this.state.snackbar.isOpen}
        			message={this.state.snackbar.message}
        			autoHideDuration={1000}
		        	onRequestClose={this.handleSnackbarClose}/>
			</main>
		)
	}
}