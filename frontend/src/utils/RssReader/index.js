import axios from 'axios';

import API from '../../api.js';

export default class RssReader {

	static getAllWebsites(){
		let websiteUrl = API.websiteUrl().list();
		return axios.get(websiteUrl)
			.then((data) => data.data);
	}

	static getAllFeeds(websiteId){
		let feedsUrl = API.feedsUrl(websiteId).list();
		return axios.get(feedsUrl)
			.then((data) => data.data);
	}

	static updateFeeds(websiteId){
		let updateFeedsUrl = API.feedsUrl(websiteId).update();
		return axios.get(updateFeedsUrl)
			.then((data) => data.data);
	}

	static addWebsite(title, url){
		let websiteUrl = API.websiteUrl().list();
		return axios.post(websiteUrl, {
			title: title,
			url: url,
		})
		.then((data) => data.data);
	}

	static deleteWebsite(websiteId){
		let websiteUrl = API.websiteUrl().detail(websiteId);
		return axios.delete(websiteUrl)
			.then((data)=> data.data);
	}
}


