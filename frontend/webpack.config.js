const path = require('path');

const ROOT_DIR = path.resolve(__dirname);
const SRC_DIR = path.join(ROOT_DIR, 'src');
const DIST_DIR = path.join(ROOT_DIR, 'dist');

module.exports = {
	entry: path.join(SRC_DIR, 'index.js'),

	output: {
		path: DIST_DIR,
		filename: 'app.js'
	},

	module: {
    	rules: [
      		{
				test: /\.(js|jsx)$/,
				exclude: /node_moudles/,
				use:[
					{
						loader: 'babel-loader',
						options: {
							presets: ['env', 'react']
						}
					}
				]
			},
			{
				test: /\.scss$/,
				exclude: /node_moudles/,
				use : [
						{
							loader: 'style-loader'
						},
						{
							loader: 'css-loader',
							options: {
								module: true,
								localIdentName: '[name]__[local]___[hash:base64:5]',
								minimize: true
							}
						}, 
						{
							loader: 'sass-loader'
						}
				]
			}
    	]
  },

}